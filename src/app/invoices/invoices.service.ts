import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class InvoicesService {

invoicesObservable;
  getInvoices(){
  
 // this.invoicesObservable = this.af.database.list('/users');
 this.invoicesObservable = this.af.database.list('/invoices').map(
     invoices =>{
       invoices.map(
         invoice => {
           invoice.posTitles = [];
           for(var p in invoice.posts){
               invoice.posTitles.push(
               this.af.database.object('/posts/' + p)              )
           }
          }
        );
        return invoices;
        
      }
    )
    

    //this.usersObservable = this.af.database.list('/invoices');
    return this.invoicesObservable;
  }
    addinvoice(invoice){
    this.invoicesObservable.push(invoice);//push is a function on array, adds object in end of array.
}
  

 updateinvoice(invoice){
let invoiceKey = invoice.$key;
let invoiceData = {name:invoice.name,amount:invoice.amount};
this.af.database.object('/invoices/' + invoiceKey).update(invoiceData);
}
  deleteinvoice(invoice){
let invoiceKey = invoice.$key;
this.af.database.object('/invoices/' + invoiceKey).remove();
    
  }*/
  //constructor(private _http:Http) { }
constructor(private af:AngularFire) {}
}


