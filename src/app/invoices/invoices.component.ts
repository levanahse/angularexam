import { Component, OnInit } from '@angular/core';
import {InvoicesService} from './invoices.service'

@Component({
  selector: 'jce-invoices',
  templateUrl: './invoices.component.html',
  
   styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50; 
  `]
})
export class InvoicesComponent implements OnInit {
  
  isLoading:Boolean = true;

  invoices;
  currentInvoice;
  select(invoice){
  this.currentInvoice = invoice; }

  constructor(private _invoicesService:InvoicesService) {  this.invoices = this._invoicesService.getInvoices();
 }

  ngOnInit() {
   this._invoicesService.getInvoices()

		    .subscribe(invoices => {this.invoices = invoices;
                              this.isLoading = false});
  }


}

