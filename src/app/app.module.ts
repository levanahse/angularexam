import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import { AngularFireModule } from 'angularfire2';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import {UsersService} from './users/users.service';
import {ProductsService} from './products/products.service';
import {InvoicesService} from './invoices/invoices.service';

import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './product/product.component';
import { InvoiceComponent } from './invoice/invoice.component';

//import { ProductComponent } from './product/product.component';
//import { UserFormComponent } from './user-form/user-form.component';


export const firebaseConfig = {
    apiKey: "AIzaSyBrMKXyox0EicUsZ_Cxwr0wwpMWAFa_CWo",
    authDomain: "mivhanmevouzarot.firebaseapp.com",
    databaseURL: "https://mivhanmevouzarot.firebaseio.com",
    storageBucket: "mivhanmevouzarot.appspot.com",
    messagingSenderId: "83032385641"
 }

const appRoutes:Routes = [ //building routes according to url entered.
  {path:'users',component:UsersComponent},//when relize path is users load user component.
  {path:'posts',component:PostsComponent},//when relize path is posts load post component.
  {path:'products',component:ProductsComponent},//when relize path is posts load post component.
  {path:'invoices',component:InvoicesComponent},//when relize path is posts load invoice component.
  {path:'invoice-form',component:InvoiceFormComponent},//when relize path is posts load invoice component.

  {path:'',component:InvoiceFormComponent},// when no path inserted load invoice component as default. 
  {path:'**',component:PageNotFoundComponent}//when path doesn't exist, load error component.
]


@NgModule({
  declarations: [
    AppComponent,
    InvoicesComponent,
    InvoiceFormComponent,
    UsersComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    PageNotFoundComponent,
    UserFormComponent,
    ProductsComponent,
    ProductComponent,
    InvoiceComponent,
    
  
 
  
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [InvoicesService,ProductsService], //UsersService
  bootstrap: [AppComponent]
})
export class AppModule { }