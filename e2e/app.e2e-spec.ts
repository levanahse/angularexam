import { AngProjPage } from './app.po';

describe('ang-proj App', function() {
  let page: AngProjPage;

  beforeEach(() => {
    page = new AngProjPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
